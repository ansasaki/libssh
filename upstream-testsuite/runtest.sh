#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/libssh/Sanity/upstream-testsuite
#   Description: The tests runs the upstream testsuite
#   Author: Jakub Jelen <jjelen@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="libssh"
REQUIRES="rpm-build libcmocka-devel cmake doxygen gcc-c++ openssl-devel
          zlib-devel krb5-devel openssh-clients
          pam_wrapper socket_wrapper nss_wrapper uid_wrapper
          openssh-server nmap-ncat checkpolicy policycoreutils"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlRun "rlFileBackup /etc/group"
        rlRun "rlFileBackup /etc/gshadow"
        rlRun "rlFileBackup /etc/passwd"
        rlRun "rlFileBackup /etc/shadow"
        rlRun "useradd -m test_user" 0 "Create a test user"
        rlRun "echo "password" | passwd --stdin test_user" 0 "Setting password for first test user"
        # -----------------------------------------------------------------
        # Should be removed when the test suite can run with default policy
        if rlIsRHEL '<8.3'; then
            rlRun "checkmodule -M -m -o my-sshd.mod my-sshd.te"
            rlRun "semodule_package -o my-sshd.pp -m my-sshd.mod"
            rlRun "semodule -X300 -i my-sshd.pp"
            rlRun "SEBOOL_CHROOT=$( getsebool ssh_chroot_rw_homedirs | awk '{print $3}' )"
            rlRun "setsebool ssh_chroot_rw_homedirs on"
            rlRun "SEBOOL_MMAP=$( getsebool domain_can_mmap_files | awk '{print $3}' )"
            rlRun "setsebool domain_can_mmap_files on"
        fi
        # -----------------------------------------------------------------
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlRun "rlFileBackup --clean --missing-ok ~/.ssh/"
        rlRun "rm -rf ~/.ssh/"
        rlRun "rlFetchSrcForInstalled $PACKAGE"
        rlRun "rpm --define \"_topdir $TmpDir\" -ivh ./$PACKAGE*.rpm"
        rlRun "rpmbuild --define \"_topdir $TmpDir\" -bp $TmpDir/SPECS/libssh.spec"
        rlRun "cd $TmpDir/BUILD/libssh-*/"
        rlRun "mkdir -p build && cd build"
        rlRun "chown test_user:test_user -R $TmpDir" 0 "Give ownership of files to test user"
    rlPhaseEnd

    rlPhaseStartTest "Run all tests"
        rlRun "runuser -u test_user -- cmake -DUNIT_TESTING=ON -DDEBUG_PACKET=ON \
                -DWITH_SFTP=ON -DWITH_SERVER=ON -DSERVER_TESTING=ON \
                -DCLIENT_TESTING=ON .."
        rlRun "runuser -u test_user -- make && ctest --output-on-failure"
        # Submit the full test log as an attachment
        rlRun "mv Testing/Temporary/LastTest.log all.log"
        # on failure try to re-run the test with more verbose logging
        if ! rlGetPhaseState; then
            rlRun "runuser -u test_user -- make && LIBSSH_VERBOSITY=4 ctest --output-on-failure"
            rlRun "mv Testing/Temporary/LastTest.log all-verbose.log"
        fi
    rlPhaseEnd

    rlPhaseStartTest "Make sure OpenSSL API is used for the KDF (#1591672)"
        # Note, that it is not simple to detect in runtime, but we can check
        # generated config.h for the variable HAVE_OPENSSL_EVP_KDF_CTX_NEW_ID,
        # which internally differentiates if the new API is used or not
        rlAssertGrep "#define HAVE_OPENSSL_EVP_KDF_CTX_NEW_ID 1" config.h
    rlPhaseEnd

    rlPhaseStartTest "opts.global_knownhosts checking - bz1649321"
        rlAssertGrep "OK .* torture_knownhosts_host_exists_global" all.log
    rlPhaseEnd

    rlPhaseStartTest "proxycommand file descriptor problem - bz1649319"
        rlAssertGrep "OK .* torture_packet" all.log
        rlAssertGrep "OK .* torture_options_set_proxycommand" all.log
    rlPhaseEnd

    rlPhaseStartTest "Do not use MD5 in Fips mode for fingerprints - bz1685903"
        rlAssertGrep "OK .* torture_sha1_hash" all.log
        rlAssertGrep "OK .* torture_sha256_hash" all.log
        rlAssertGrep "OK .* torture_sha256_fingerprint" all.log
    rlPhaseEnd

    if ! rlIsRHEL '<8.3'; then
        rlPhaseStartTest "create directories recursively - bz1733914"
            rlAssertGrep "OK .* torture_knownhosts_new_file" all.log
            rlRun "cat >$TmpDir/sums <<_EOF
adceb93515b63acbc10411839db0095ed42a693c736baae64617e8488eaabb36  tests/client/torture_knownhosts_verify.c
_EOF"
            rlRun "pushd $TmpDir/BUILD/libssh-*/"
            rlRun "sha256sum -c $TmpDir/sums"
            rlRun "popd"
        rlPhaseEnd
    fi

    rlPhaseStartTest "libssh testsuite fails in on x86_64 - bz1688842"
        rlRun "runuser -u test_user -- ctest -R torture_auth --output-on-failure" 0
        rlRun "mv Testing/Temporary/LastTest.log torture_auth.log"
    rlPhaseEnd

    if ! rlIsRHEL '<8.3'; then
        rlPhaseStartTest "Sanitize location in SCP - bz1781782"
            rlAssertGrep "OK .* torture_ssh_quote_file_name" all.log
            rlAssertGrep "OK .* torture_scp_upload_newline" all.log
            rlRun "cat >$TmpDir/sums <<_EOF
fd490d9ae717cf004b7c3b7c61b954f79ef49e78c0dce51122d8a0b9b527ee81  tests/client/torture_scp.c
d7daec7616431f055c9ce305eb7f5da6a88989d70a8da7b56e493b98ee9edcef  tests/unittests/torture_misc.c
_EOF"
            rlRun "pushd $TmpDir/BUILD/libssh-*/"
            rlRun "sha256sum -c $TmpDir/sums"
            rlRun "popd"
        rlPhaseEnd
    fi

    rlPhaseStartCleanup
        rlRun "rlBundleLogs libssh *.log"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
        rlRun "userdel -r test_user" 0 "Removing test user"
        # -----------------------------------------------------------------
        # Should be removed when the test suite can run with default policy
        if rlIsRHEL '<8.3'; then
            rlRun "setsebool ssh_chroot_rw_homedirs $SEBOOL_CHROOT"
            rlRun "setsebool domain_can_mmap_files $SEBOOL_MMAP"
            rlRun "semodule -X300 -r my-sshd"
        fi
        # -----------------------------------------------------------------
        rlRun "rlFileRestore"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
