libssh
======

Tests for libssh library.

Introduction:
-------------

The tests stored in this repository use the fmf format and are meant to be
found and executed through tmt.

Requirements:
-------------

- [fmf](https://fmf.readthedocs.io/en/latest/index.html)
- [tmt](https://tmt.readthedocs.io/en/latest/index.html)

How to run:
-----------

The recommended way is to run inside a container:

```
$ tmt run --all provision --how container --image fedora
```

How to integrate into a CI:
-------------------------

To integrate into a CI system, the CI machine needs to have ``tmt`` installed and
a test plan.

To create a test plan use ``tmt``:

```
$ tmt init
$ tmt plan create libssh
```

Edit the test plan to point directly to the repository. The content could be:
```
summary: libssh tests
discover:
    how: fmf
    repository: https://gitlab.com/redhat-crypto/tests/libssh.git
execute:
    how: beakerlib
```

Then the test script can run:

```
$ tmt run --all provision --how local
```

