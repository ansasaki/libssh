#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /key-file-formats
#   Description: Test supported key formats and file formats
#   Author: Anderson Toshiyuki Sasaki <ansasaki@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2020 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="libssh"
PACKAGES="libssh curl openssh-server openssh-clients crypto-policies"

POLICY_FILE="/etc/crypto-policies/back-ends/libssh.config"

FILESIZE=1

# For OpenSSH, 'PEM' means the legacy PEM format
FILE_FORMATS=("PKCS8" "OpenSSH" "PEM")
TESTED_ALGORITHMS=("ecdsa" "rsa" "ed25519" "dsa")
WITH_PASSPHRASE=("without" "with")

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm --all
        rlImport crypto/fips || rlDie "Cannot import library(crypto/fips)"

        # Check if FIPS mode is enabled
        fipsIsEnabled && FIPS_ENABLED=1 ||
        if [[ $? -eq 2 ]]; then
            rlDie "FIPS mode not correctly enabled";
        else
            FIPS_ENABLED=0
        fi;

        if [[ $FIPS_ENABLED -eq 0 ]]; then
            # Backup crypto-policies profile
            POLICY_BACKUP="$(update-crypto-policies --show)"
            rlRun "update-crypto-policies --set DEFAULT" 0 "Set crypto-policies profile to DEFAULT"
        else
            TESTED_ALGORITHMS=("ecdsa" "rsa");
        fi;

        # Create temporary directory
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"

        rlRun "rlFileBackup --clean /etc/ssh"
        rlRun "rm -rf /etc/ssh/ssh_host_*" 0 "Cleanup server keys"

        # Generate server key
        rlRun "ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''" 0 "Generate server key"
        rlAssertExists "/etc/ssh/ssh_host_rsa_key"
        rlAssertExists "/etc/ssh/ssh_host_rsa_key.pub"

        # Add testuser, the owner of the file to be copied
        rlRun "useradd -m testuser"

        # Generate the test file and calculate hash to compare integrity
        rlRun "su - testuser -c 'dd if=/dev/zero of=testfile bs=1M count=$FILESIZE'" 0 "Creating $FILESIZE MB large test file"
        SUM=`sha256sum /home/testuser/testfile | cut -d ' ' -f 1`

        # Start the sshd server
        rlRun "rlServiceStart sshd"
    rlPhaseEnd

    for algorithm in "${TESTED_ALGORITHMS[@]}"; do
        if [[ $algorithm = "dsa" ]]; then
            rlPhaseStartTest "Set LEGACY policy to allow DSA to be used"
                # Set crypto-policies profile to LEGACY and restart the
                # server
                rlRun "rlServiceStop sshd" 0 "Stop SSH server"
                rlRun "update-crypto-policies --set LEGACY" 0 \
                      "Set crypto-policies profile to LEGACY"
                rlRun "rlServiceStart sshd" 0 "Start SSH server"
            rlPhaseEnd
        fi;

        for format in "${FILE_FORMATS[@]}"; do
            for with_passphrase in "${WITH_PASSPHRASE[@]}"; do
                rlPhaseStartTest "Test with $algorithm key in $format format $with_passphrase passphrase"

                    if [[ $format = "OpenSSH" ]]; then
                        argument=""
                    else
                        argument="-m $format"
                    fi;

                    if [[ $with_passphrase = "with" ]]; then
                        passphrase="123456"
                        pass_arg="--pass $passphrase"
                    else
                        passphrase=""
                        pass_arg=""
                    fi;

                    # Generate user key
                    rlRun "rlFileBackup --missing-ok --clean ~/.ssh"
                    rlRun "rm -rf ~/.ssh/*" 0 "Cleanup ~/.ssh"
                    rlRun "ssh-keygen $argument -t $algorithm -f ~/.ssh/id_$algorithm -N '$passphrase'" 0 \
                          "Generate $algorithm key in $format format $with_passphrase passphrase"

                    # Put the server public key in the user known_hosts file
                    rlRun "echo 'localhost $(cat "/etc/ssh/ssh_host_rsa_key.pub")' > \
                           ~/.ssh/known_hosts" 0 "Add server key to known hosts"

                    # Authorize the user key to allow authentication without prompting
                    rlRun "mkdir -p /home/testuser/.ssh" 0 "Create user .ssh directory"
                    rlRun "cat ~/.ssh/id_$algorithm.pub > /home/testuser/.ssh/authorized_keys" 0 "Authorize user key"
                    rlRun "chown -R testuser.testuser /home/testuser/.ssh/" 0 "Give ownership of the files to the user"

                    # Download the file and verify its integrity
                    rlRun -s "curl -o ./sftp_file -u testuser: \
                              --key ~/.ssh/id_$algorithm $pass_arg\
                              --pubkey ~/.ssh/id_$algorithm.pub \
                              sftp://localhost/home/testuser/testfile" 0 \
                              "Download test file"
                    rlAssertExists "sftp_file"
                    SFTPSUM=`sha256sum ./sftp_file | cut -d ' ' -f 1`
                    rlAssertEquals "Checking that the file was properly downloaded" $SUM $SFTPSUM
                    rlRun "rm -f ./sftp_file" 0 "Remove file"
                rlPhaseEnd
            done;
        done;
    done;

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
        rlRun "userdel -r --force testuser" 0 "Remove test user"
        rlRun "rlFileRestore"
        if [[ $FIPS_ENABLED -eq 0 ]]; then
            rlRun "update-crypto-policies --set $POLICY_BACKUP" 0
        fi;
        rlRun "rlServiceRestore sshd"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
